package com.familyhub;

import android.os.IBinder;
import android.os.Looper;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.os.Process;
import android.os.SystemClock;

import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.AuthFailureError;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Service;
import android.widget.Toast;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import android.app.Notification;
import androidx.core.app.NotificationManagerCompat;

public class MyBackgroundService extends Service{
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
      super.onStartCommand(intent, flags, startId);

      final Context application_context = this;

      SharedPreferences sp = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
      String userId = sp.getString("UserId","");

      if(userId != ""){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://vps-10a8b356.vps.ovh.net:3000/api/notification/" + userId;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
          new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
              try{
                for (int i=0; i < response.length(); i++) {
                    JSONObject notif = response.getJSONObject(i);

                    if(notif.getString("type").equals("Task")){
                      NotificationCompat.Builder builder = new NotificationCompat.Builder(application_context, "TEST")
                        .setSmallIcon(R.drawable.splashscreen_image)
                        .setContentTitle("Nouvelle Tâche")
                        .setContentText("Vous venez de recevoir une nouvelle tâche !")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                      NotificationManagerCompat notificationManager = NotificationManagerCompat.from(application_context);
                      notificationManager.notify(1, builder.build());                 
                    }
                }
              }catch (Exception ex) {
                ex.printStackTrace();
              }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        queue.add(jsonArrayRequest);
      }

      return START_STICKY;
    }

    @Override
    public void onDestroy(){
      super.onDestroy();

      Intent broadcastIntent = new Intent(this, MyBackgroundService.class);
      startService(broadcastIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
      return null;
    }
}