import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, KeyboardAvoidingView, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'

import FirstStart from './src/components/FirstStart/FirstStart';
import Welcome from './src/components/Auth/Welcome';
import MainMenu from './src/components/App/MainMenu';

export default class App extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      actual_part: 'welcome'
    }
  }

  async componentDidMount(){
    try{
      const first_start = await AsyncStorage.getItem('FirstStart');
      if(first_start == null){
        this.setState({actual_part: 'first_start'});
      }

      const user = await AsyncStorage.getItem('UserData');

      if(user != null){
        this.handleAppLaunch();
      }
    } catch(error){
        Alert.alert("Error getting data !" + error)
    }
  }

  async handleSkipFirstStart(){
    this.setState({actual_part: 'welcome'});

    try{
      await AsyncStorage.setItem(
        'FirstStart',
        'false'
      );
    } catch(error){}
  }

  handleAppLaunch(){
    this.setState({actual_part: 'app'});
  }

  handleDisconnect(){
    this.setState({actual_part: 'welcome'})
  }

  render(){
    return (
      <KeyboardAvoidingView
        style={styles.container}
      >
        <View style={styles.container}>
          <StatusBar style="auto" />
          {this.state.actual_part == "first_start"?<FirstStart skip_handler={this.handleSkipFirstStart.bind(this)}/>:<></>}
          {this.state.actual_part == "welcome"?<Welcome app_handler={this.handleAppLaunch.bind(this)}/>:<></>}
          {this.state.actual_part == "app"?<MainMenu disconnect_handler={this.handleDisconnect.bind(this)}/>:<></>}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
});
