import React, { useState } from 'react';
import API from '../api';

import { View, Text, Alert, Image, TouchableOpacity, ScrollView, SafeAreaView, ToastAndroid } from 'react-native';
import { WingBlank, Button } from '@ant-design/react-native';
import Dialog from "react-native-dialog";
import AsyncStorage from '@react-native-community/async-storage'
import Clipboard from '@react-native-clipboard/clipboard';
import QRCode from 'react-native-qrcode-svg';

//Elements

export function MemberAddModal(props){
    const [username, setUsername] = useState("");

    const handleCancel = () => {
        props.handle_visibility();
    };
    
    const handleAdd = () => {
        API.post('/api/members', {
            username: username,
            role_name: "Enfant"
        })
        .then(() => {
            props.handle_reload();
            setUsername("");
        })
        .catch((err) => {  
            console.log("Members Add Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Erreur serveur");
                    break;
                case 400 : Alert.alert("Champs vide ou invalide")
                    break;
                default : Alert.alert("Erreur inconnue")
                    break;
            }
        })

        props.handle_visibility();
    };

    return (
        <View>
          <Dialog.Container visible={props.visible}>
            <Dialog.Title style={{textAlign:'center', marginBottom:5}}>Ajouter un nouveau membre</Dialog.Title>
            <Dialog.Input onChangeText={setUsername} style={{borderBottomWidth:1, fontSize:19, marginTop:5}} label={"Nom du membre"} value={username}></Dialog.Input>
            <Dialog.Button label="Annuler" onPress={handleCancel}/>
            <Dialog.Button label="Ajouter" onPress={handleAdd}/>
          </Dialog.Container>
        </View>
    );
}

export function MemberView(props){
    const handleRemove = () => {
        API.delete('/api/members/'+props.member.id)
        .then((res) => {
            props.handle_reload();
        })
        .catch((err) => {  
            console.log("Members Delete Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    };

    const handleRole = () => {
        let role = props.member.name=="Parent"?"child":"parent"
        API.put('/api/members/'+props.member.id+'/'+role)
        .then((res) => {
            props.handle_reload();
        })
        .catch((err) => {  
            console.log("Members Role Change Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    };

    return (
        <View style={{flexDirection:'column', marginVertical: 5, paddingVertical:5, paddingHorizontal:5, backgroundColor: 'rgba(255,255,255,0.5)', borderRadius:5}}>
            <Text style={{fontSize:18, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1, textAlign:'center'}}>{props.member.username} - {props.member.name} {props.member.creator?"- Createur":""}</Text>
            {!props.member.creator?<>
            <Text style={{fontSize:18, color:'#4F4F4F', textAlign:'center'}}>{props.member.invite_code}</Text>
            <View style={{flex: 1, flexDirection:'row', paddingHorizontal:5, justifyContent:'space-around', marginTop:5}}>
                <TouchableOpacity onPress={props.handle_scan} style={{display:'flex', alignItems:'center'}}>
                    <Image 
                        style={{width:30, height: 30, alignItems:'center'}}
                        resizeMode={'contain'}
                        source={require('../../assets/scan_logo.png')}
                    />
                    <Text>Scan</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleRole} style={{display:'flex', alignItems:'center'}}>
                    <Image 
                        style={{width:30, height: 30, alignItems:'center'}}
                        resizeMode={'contain'}
                        source={require('../../assets/role_logo.png')}
                    />
                    <Text>Role</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleRemove} style={{display:'flex', alignItems:'center'}}>
                    <Image 
                        style={{width:31, height: 31, alignItems:'center'}}
                        resizeMode={'contain'}
                        source={require('../../assets/user_remove.png')}
                    />
                    <Text>Enlever</Text>
                </TouchableOpacity>
            </View></>:<></>}
        </View> 
    )
}

export class MemberSection extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            members: [],
            showQr: false,
            QRText: "null"
        }

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;

        this.loadMembers();
    }

    handleAddVisibility(){
        if(this._isMounted)
            this.setState({visible: !this.state.visible});
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    loadMembers(){
        API.get('/api/members')
        .then((res) => {
            if(this._isMounted)
                this.setState({members: res.data})
        })
        .catch((err) => {  
            console.log("Members Load Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Erreur serveur");
                    break;
                case 400 : Alert.alert("Champs vide ou invalide")
                    break;
                default : Alert.alert("Erreur inconnue")
                    break;
            }
        })
    }

    //Showing QrCode to User
    showScan(value, event){
        API.put('/api/members/'+value.id+'/token')
        .then((res) => {
            if(this._isMounted){
                this.setState({showQr: true})
                this.setState({QRText: res.data.connection_token})
            }
        })
        .catch((err) => {  
            console.log("Members Token Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    //Hidding QrCode to User
    hideScan(){
        if(this._isMounted){
            this.setState({showQr: false})
            this.setState({QRText: "null"})
        }
    }

    render(){
        return (
            <View style={{flex: 1, flexDirection:'column', alignItems:'center'}}>
                <View style={{width:'100%', flexDirection:'row', justifyContent:'center'}} >
                    <Text style={{fontSize:20, marginVertical:15, color:'#4F4F4F', fontWeight:'bold', letterSpacing:0.5}}>Membres de la Famille</Text>
                    <WingBlank style={{flex: 0.8}}></WingBlank>                 
                    <TouchableOpacity onPress={this.handleAddVisibility.bind(this)}>
                        <Image 
                            style={{width:30, height: 30, alignItems:'center', marginTop:20}}
                            resizeMode={'contain'}
                            source={require('../../assets/create_logo.png')}
                        />
                    </TouchableOpacity>
                    <MemberAddModal handle_visibility={this.handleAddVisibility.bind(this)} handle_reload={this.loadMembers.bind(this)} visible={this.state.visible}/>
                </View>
                <View style={{width:'100%', justifyContent:'center'}}>
                    {this.state.members.map( (member, index) => {
                            return <MemberView key={index} member={member} handle_scan={this.showScan.bind(this, member)} handle_reload={this.loadMembers.bind(this)}/>
                        })}       
                </View>
                <Dialog.Container visible={this.state.showQr} contentStyle={{backgroundColor:'#A9F2E8', display:'flex', alignItems:'center'}}>
                    <Dialog.Title style={{textAlign:'center', color:"#4F4F4F", fontWeight:'bold'}}>Scanner le code QR ci-dessous avec un autre appareil !</Dialog.Title>
                    <View style={{display:'flex', alignItems:'center'}}>
                        <QRCode
                            value={this.state.QRText}
                            backgroundColor={"transparent"}
                            size={200}
                        />
                    </View>
                    <Dialog.Button onPress={this.hideScan.bind(this)} style={{color:'black'}} label="Retour" />
                </Dialog.Container>
            </View>
        )
    }
}

export class PreferenceSection extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            preferences: null
        }

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;

        this.loadPreferences();
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    loadPreferences(){
        API.get('/api/preferences')
        .then((res) => {
            if(this._isMounted)
                this.setState({preferences: res.data})
        })
        .catch((err) => {  
            console.log("Preferences Load Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Erreur serveur");
                    break;
                case 400 : Alert.alert("Champs vide ou invalide")
                    break;
                default : Alert.alert("Erreur inconnue")
                    break;
            }
        })
    }

    render(){
        return (
            <View style={{flex: 1, flexDirection:'column', alignItems:'center'}}>
                <View style={{width:'100%', flexDirection:'row', justifyContent:'center'}} >
                    <Text style={{fontSize:20, marginVertical:15, color:'#4F4F4F', fontWeight:'bold', letterSpacing:0.5}}>Preferences</Text>
                    <WingBlank style={{flex: 0.95}}></WingBlank>  
                </View>
                <View style={{width:'100%', justifyContent:'center'}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize:15, marginLeft:20, color:'#4F4F4F', letterSpacing:1}}>Nom de famille :</Text>  
                        <Text style={{fontSize:17, marginLeft:20, color:'#4F4F4F', fontWeight:'700'}}>{this.state.preferences != null?this.state.preferences.family_name:""}</Text> 
                    </View>                  
                </View>
            </View>
        )
    }
}

export class RetrieveSection extends React.Component{
    constructor(props) {
        super(props);

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    getRetrieveCode(){
        API.get('/api/families/retrieve')
        .then((res) => {
            if(this._isMounted){
                Clipboard.setString(res.data.retrieve_code)
                ToastAndroid.show("Code copier dans le presse papier !", ToastAndroid.SHORT);
                Alert.alert(res.data.retrieve_code, "Utiliser ce code afin de récupérer votre famille, veillez à le noter discrètement et ne pas le divulguer aux autres !",)
            }
        })
        .catch((err) => {  
            console.log("Family Retrieve Code Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    }

    render(){
        return (
            <View style={{flex: 1, flexDirection:'column', alignItems:'center'}}>
                <View style={{width:'100%', flexDirection:'row', justifyContent:'center'}} >
                    <Text style={{fontSize:20, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:0.5}}>Code de récupération</Text>
                    <WingBlank style={{flex: 0.95}}></WingBlank>  
                </View>
                <View style={{width:'100%', justifyContent:'center'}}>
                    <Button onPress={this.getRetrieveCode.bind(this)} style={{backgroundColor: '#334668', marginVertical: 10}}><Text style={{color:'white'}}>Générer un nouveau code</Text></Button>                 
                </View>
            </View>
        )
    }
}

export function Disconnect(props){
    const disconnect = () => {
        API.delete('/api/sessions')
        .then( async (res) => {
            await AsyncStorage.removeItem('UserData');
            props.disconnect_handler();
        })
        .catch((err) => {  
            console.log("Members Disconnect Error : " + err.response.data.message)
            
            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    }

    return (
        <View style={{flex: 1, flexDirection:'column', alignItems:'center'}}>
            <View style={{width:'100%', flexDirection:'row', justifyContent:'center'}} >
                <Text style={{fontSize:20, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:0.5}}>Session</Text>
                <WingBlank style={{flex: 0.95}}></WingBlank>  
            </View>
            <View style={{width:'100%', justifyContent:'center'}}>
            <Button onPress={disconnect} style={{backgroundColor: '#334668', marginVertical: 10}}><Text style={{color:'white'}}>Se Déconnecter</Text></Button>                 
            </View>
        </View>
    )
}

//Base Class

export default class Parameter extends React.Component{
    constructor(props) {
        super(props);

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    handleDisconnect(){
        this.props.disconnect_handler();
    }

    render(){
        return (
            <SafeAreaView style={{flex: 1, flexDirection:'column', alignItems:'center'}}>
                <ScrollView>
                    <PreferenceSection />
                    <MemberSection />
                    {this.props.member.creator?<RetrieveSection />:<></>}
                    <Disconnect disconnect_handler={this.handleDisconnect.bind(this)}/>
                </ScrollView>
            </SafeAreaView>
        )
    }
}