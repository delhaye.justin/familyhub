import React from 'react';
import API from '../api';

import { View, Text, Image, TouchableOpacity, Alert} from 'react-native';
import { WingBlank } from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage'

import Home from './Home';
import Message from './Message';
import Parameter from './Parameter';
import Shopping from './Shopping';
import Event from './Event';
import Task from './Task';

//Basic Layout

export function Header(props){
    const disconnect = () => {
        API.delete('/api/sessions')
        .then( async (res) => {
            await AsyncStorage.removeItem('UserData');
            props.disconnect_handler();
        })
        .catch((err) => {  
            console.log("Members Disconnect Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    return (
        <View style={{flex: .12, flexDirection:'row'}}>
            <WingBlank style={{flex: 0.1}}></WingBlank>
            <View style={props.member_type == "Parent"?{flex: 0.8, flexDirection:'row', justifyContent:'center', alignItems:'center'}:{flex: 1, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                {props.selected == 0?<Text style={{fontSize:22, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Menu Principal</Text>:<></>}
                {props.selected == 1?<Text style={{fontSize:22, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Calendrier</Text>:<></>}
                {props.selected == 2?<Text style={{fontSize:22, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Messages</Text>:<></>}
                {props.selected == 3?<Text style={{fontSize:22, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Courses</Text>:<></>}
                {props.selected == 4?<Text style={{fontSize:22, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Tâches</Text>:<></>}
                {props.selected == 5?<Text style={{fontSize:22, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Paramètres</Text>:<></>}
            </View>
            {props.member_type == "Parent"?
            <TouchableOpacity style={{flex: 0.2, justifyContent:'center', alignItems:'center', marginTop:20 }} onPress={props.handle_parameter}>
                <Image 
                    style={{width:30}}
                    resizeMode={'contain'}
                    source={require('../../assets/parameter_logo.png')}
                />
            </TouchableOpacity>:
            <TouchableOpacity style={{flex: 0.2, justifyContent:'center', alignItems:'center', marginTop:20 }} onPress={disconnect}>
                <Image 
                    style={{width:30}}
                    resizeMode={'contain'}
                    source={require('../../assets/logout_icon.png')}
                />
            </TouchableOpacity>}
        </View>
    )
}

export function Content(props){
    return (
        <View style={{flex: .8, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
            <View style={{flex: 1, flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                {props.selected == 0?<Home />:<></>}
                {props.selected == 1?<Event member={props.member}/>:<></>}
                {props.selected == 2?<Message />:<></>}
                {props.selected == 3?<Shopping member={props.member}/>:<></>}
                {props.selected == 4?<Task member={props.member}/>:<></>}
                {props.selected == 5?<Parameter member={props.member} disconnect_handler={props.disconnect_handler}/>:<></>}
            </View>
        </View>
    )
}

export function Footer(props){
    return (
        <View style={{flex: .08, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
            <View style={props.selected == 2?{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 1)', justifyContent:'center', alignItems:'center'}:{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 0.9)', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={props.handle_message}>
                    <Image 
                        style={{width:30 }}
                        resizeMode={'contain'}
                        source={require('../../assets/message_logo.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={props.selected == 1?{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 1)', justifyContent:'center', alignItems:'center'}:{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 0.9)', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={props.handle_calendar}>
                    <Image 
                        style={{width:30}}
                        resizeMode={'contain'}
                        source={require('../../assets/calendar_logo.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={props.selected == 0?{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 1)', justifyContent:'center', alignItems:'center'}:{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 0.9)', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={props.handle_home}>
                    <Image 
                        style={{width:40}}
                        resizeMode={'contain'}
                        source={require('../../assets/home_logo.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={props.selected == 3?{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 1)', justifyContent:'center', alignItems:'center'}:{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 0.9)', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={props.handle_shopping}>
                    <Image 
                        style={{width:30}}
                        resizeMode={'contain'}
                        source={require('../../assets/shoppping_logo.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={props.selected == 4?{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 1)', justifyContent:'center', alignItems:'center'}:{flex: .2, height:'100%', backgroundColor:'rgba(28, 53, 107, 0.9)', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={props.handle_task}>
                    <Image 
                        style={{width:25}}
                        resizeMode={'contain'}
                        source={require('../../assets/task_logo.png')}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

//Base Class

export default class MainMenu extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            actual_page: 0
        }

        this._isMounted = false;
    }

    handleMenuChange(value){
        this.setState({actual_page: value});
    }

    async componentDidMount(){
        this._isMounted = true;

        try{
            const user = await AsyncStorage.getItem('UserData');
            let parseUser = JSON.parse(user);

            API.get('/api/members')
            .then(async (res) => {
                let newUserData = res.data.filter(user => user.id == parseUser.id)[0]

                try{
                    await AsyncStorage.setItem(
                        'UserData',
                        JSON.stringify(newUserData)
                    );
    
                    if(this._isMounted){
                        this.setState({user: newUserData});
                    }
                } catch(error){
                    console.log("Storage Error : " + error)
                }
            })
            .catch((err) => {  
                console.log("Members Load Error : " + err.response.data.message)
                
                Alert.alert("Erreur inconnue !")
            })

            if(this._isMounted){
                this.setState({user: JSON.parse(user)});
            }
        } catch(error){
            Alert.alert("Erreur inconnue !")
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    handleDisconnect(){
        this.props.disconnect_handler();
    }

    render(){
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1, backgroundColor:'#FFDC53'}}>
                    <Header disconnect_handler={this.handleDisconnect.bind(this)} selected={this.state.actual_page} handle_parameter={this.handleMenuChange.bind(this, 5)} member_type={this.state.user != null?this.state.user.name:"Enfant"}/>
                    <Content disconnect_handler={this.handleDisconnect.bind(this)} member={this.state.user} selected={this.state.actual_page}/>
                    <Footer selected={this.state.actual_page} handle_home={this.handleMenuChange.bind(this, 0)} handle_message={this.handleMenuChange.bind(this, 2)} handle_calendar={this.handleMenuChange.bind(this, 1)} handle_shopping={this.handleMenuChange.bind(this, 3)} handle_task={this.handleMenuChange.bind(this, 4)}/>
                </View>
            </View>
        )
    }
}