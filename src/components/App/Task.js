import React, { useState } from 'react';
import API from '../api';
import DatePicker from 'react-native-datepicker'
import DateTimePicker from '@react-native-community/datetimepicker';

import { View, Text, Alert, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';

//Elements

export function TaskView(props){
    const deleteTask = () => {
        API.delete('/api/tasks/'+props.task.id)
        .then((res) => {
            props.handle_reload();       
        })
        .catch((err) => {
            console.log("Task delete Error : " + err.response.data.message)

            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    }

    const doneTask = () => {
        API.put('/api/tasks/'+props.task.id,{
            done_date: new Date()
        })
        .then((res) => {
            props.handle_reload();       
        })
        .catch((err) => {
            console.log("Task done Error : " + err.response.data.message)

            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    }

    if(props.isParent){
        return (
            <View style={{flex:1, width:"100%", marginVertical:5, backgroundColor: 'rgba(255,255,255,0.5)', padding:5}}>
                <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                    <Text style={{fontWeight:'bold', fontSize:18, flex:0.25, textAlign:'center'}}>{props.task.username}</Text>
                    <Text style={{fontWeight:'bold', fontSize:18, textAlign:'center', flex:0.5}}>{props.task.todo_date.split("T")[0].split("-")[1]}/{props.task.todo_date.split("T")[0].split("-")[2]}</Text>
                    <TouchableOpacity onPress={deleteTask} style={{alignItems:'center', flex:0.25}}>
                        <Image 
                            style={{width:30, height:30}}
                            resizeMode={'contain'}
                            source={require('../../assets/delete_logo.png')}
                        />
                    </TouchableOpacity>
                </View>  
                <View style={{flexDirection:'row', justifyContent:'space-around', alignItems:'center'}}>
                    {props.task.done_date!=undefined?
                    <Text style={{fontWeight:'bold', fontSize:18, textAlign:'center', flex:0.25}}>{props.task.done_date.split("T")[0].split("-")[1]}/{props.task.done_date.split("T")[0].split("-")[2]}</Text>
                    :<Text style={{flex:0.25}}></Text>}
                    <Text style={{fontSize:16, textAlign:'center', flex:0.5}}> {props.task.description}</Text>
                    <TouchableOpacity onPress={doneTask} style={{alignItems:'center', flex:0.25}}>
                        <Image 
                            style={{width:50, height:50}}
                            resizeMode={'contain'}
                            source={props.task.done_date!=undefined?require('../../assets/done_logo.png'):require('../../assets/notdone_logo.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }else{
        return (
            <View style={{flex:1, width:"100%", marginVertical:5, backgroundColor: 'rgba(255,255,255,0.5)', padding:5}}>
                <View style={{flexDirection:'row', justifyContent:'space-around', alignItems:'center'}}>
                    <View style={{flex:0.25, display:'flex', alignItems:'center'}}>
                        {props.task.todo_date < new Date()?
                            <Image 
                                style={{width:30, height:30}}
                                resizeMode={'contain'}
                                source={require('../../assets/warning_logo.png')}
                            />:<></>} 
                        <Text style={{fontWeight:'bold', fontSize:18, textAlign:'center'}}>{props.task.todo_date.split("T")[0].split("-")[1]}/{props.task.todo_date.split("T")[0].split("-")[2]}</Text>
                    </View>
                    <Text style={{fontSize:16, textAlign:'center', flex:0.5}}> {props.task.description}</Text>
                    <TouchableOpacity onPress={doneTask} style={{alignItems:'center', flex:0.25}}>
                        <Image 
                            style={{width:50, height:50}}
                            resizeMode={'contain'}
                            source={props.task.done_date!=undefined?require('../../assets/done_logo.png'):require('../../assets/notdone_logo.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export function TaskAddModal(props){
    const [memberId, setMemberId] = useState("");
    const [taskdesc, setTaskdesc] = useState("");
    const [taskDate, setTaskDate] = useState(new Date());

    const handleCancel = () => {
        props.handle_visibility();
    };

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || taskDate;
        setTaskDate(currentDate);
        props.handle_date();
    };
    
    const handleAdd = () => {
        API.post('/api/tasks', {
            description: taskdesc,
            date: taskDate,
            member_id: memberId!=""?memberId:props.members.filter(user => user.name == "Enfant")[0].id
        })
        .then(() => {
            props.handle_reload();
            setTaskdesc("");
        })
        .catch((err) => {  
            console.log("Task Add Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })

        props.handle_visibility();
    };

    return (
        <View>
          <Dialog.Container visible={props.visible}>
            <Dialog.Title style={{textAlign:'center', marginBottom:5}}>Ajouter une nouvelle tâche</Dialog.Title>
            <Dialog.Input onChangeText={setTaskdesc} style={{borderBottomWidth:1, fontSize:19, marginTop:5}} label={"Description de la tâche"} value={taskdesc}></Dialog.Input>
            <Text style={{fontSize:14, marginLeft:10}}>Enfant concerné</Text>
            <Picker
                onValueChange={(itemValue, itemIndex) =>
                    setMemberId(itemValue)
            }>
                {props.members.filter(user => user.name == "Enfant").map((member, index) => {
                    return <Picker.Item key={index} label={member.username} value={member.id} />
                })}
            </Picker>
            <Text style={{fontSize:17, marginTop:5, textAlign:'center'}}>Date de réalisation</Text>
            <TouchableOpacity onPress={props.handle_date} style={{alignItems:'center', backgroundColor:'#EBECF0', borderRadius:10, borderWidth:1, borderColor:"#BEBEBE", marginTop:5}}>
                <Text style={{fontSize:19}}>{taskDate.toISOString().split("T")[0]}</Text>
            </TouchableOpacity>
            {props.dateVisible?
            <DateTimePicker
                value={taskDate}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={onChange}
            />:<></>}
            <Dialog.Button label="Annuler" onPress={handleCancel}/>
            <Dialog.Button label="Ajouter" onPress={handleAdd}/>
          </Dialog.Container>
        </View>
    );
}

//Base Class

export default class Task extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            dateVisible:false,
            tasks: [],
            members: []
        }

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;

        this.loadMembers();
        this.loadTasks();
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    handleAddVisibility(){
        if(this._isMounted && this.state.members.filter(user => user.name == "Enfant").length > 0)
            this.setState({visible: !this.state.visible});
        else
            Alert.alert("Vous n'avez pas d'enfants à qui demander des tâches !")
    }

    handleDateVisibility(){
        if(this._isMounted)
            this.setState({dateVisible: !this.state.dateVisible});
    }

    loadMembers(){
        API.get('/api/members')
        .then((res) => {
            if(this._isMounted)
                this.setState({members: res.data})
        })
        .catch((err) => {  
            console.log("Members Load Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    loadTasks(){
        API.get('/api/tasks')
        .then((res) => {
            if(this._isMounted)
                this.setState({tasks: res.data})
        })
        .catch((err) => {  
            console.log("Tasks Load Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    render(){
        return (
            <SafeAreaView style={{flex: 1, width:"100%", flexDirection:'column', alignItems:'center'}}>
                <ScrollView style={{flex: 1, width:"90%"}}>
                    {this.state.tasks.map((task, index) => {
                        return <TaskView key={index} isParent={this.props.member.name=="Parent"} task={task} handle_reload={this.loadTasks.bind(this)}/>
                    })}
                </ScrollView>
                {this.props.member.name == "Parent"?
                <View style={{width:'100%', alignItems:'flex-end', padding:5, position:'absolute', bottom:0}}>
                    <TouchableOpacity onPress={this.handleAddVisibility.bind(this)}>
                        <Image 
                            style={{width:50, height: 50}}
                            resizeMode={'contain'}
                            source={require('../../assets/create_logo.png')}
                        />
                    </TouchableOpacity>
                    <TaskAddModal handle_visibility={this.handleAddVisibility.bind(this)} members={this.state.members} handle_reload={this.loadTasks.bind(this)} dateVisible={this.state.dateVisible} handle_date={this.handleDateVisibility.bind(this)} visible={this.state.visible}/>
                </View>:<></>}
            </SafeAreaView>
        )
    }
}