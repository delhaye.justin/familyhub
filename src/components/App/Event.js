import React, { useState } from 'react';
import API from '../api';
import DatePicker from 'react-native-datepicker'
import moment from 'moment';
import {CalendarList, LocaleConfig} from 'react-native-calendars';

import { View, Text, Alert, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { Button, TextareaItem } from '@ant-design/react-native';
import Dialog from "react-native-dialog";

LocaleConfig.locales['fr'] = {
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
    today: 'Aujourd\'hui'
};

LocaleConfig.defaultLocale = 'fr';

//Elements

export function EventAddModal(props){
    const actualDate = moment().format('YYYY-MM-DD');
    const [eventName, setEventName] = useState("");
    const [eventMessage, setEventMessage] = useState("");
    const [eventDate, setEventDate] = useState(actualDate);

    const handleCancel = () => {
        props.handle_visibility();
    };
    
    const handleAdd = () => {
        API.post('/api/events', {
            name: eventName,
            message: eventMessage,
            date: eventDate
        })
        .then(() => {
            props.handle_reload();
            props.handle_visibility();
            setEventName("");
            setEventMessage("");
            setEventDate("");
        })
        .catch((err) => {  
            console.log("Event Add Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    };

    return (
        <View>
          <Dialog.Container visible={props.visible}>
            <Dialog.Title style={{textAlign:'center', marginBottom:5}}>Ajouter un nouvel évenement</Dialog.Title>
            <Dialog.Input onChangeText={setEventName} style={{borderBottomWidth:1, fontSize:19, marginTop:5}} placeholder={"Nom de l'évenement"} value={eventName}></Dialog.Input>
            <TextareaItem
                style={{borderBottomWidth:1, fontSize:19, marginTop:5, marginBottom:30}}
                placeholder="Entrer une description"
                onChange={setEventMessage}
                autoHeight
            />
            <DatePicker
                style={{width: '100%', display:'flex', marginBottom:10}}
                date={eventDate}
                mode="date"
                format="YYYY-MM-DD"
                minDate="1997-01-01"
                maxDate="2099-12-31"
                useNativeDriver={true}
                customStyles={{
                    dateIcon: {
                        display:'none'
                    }
                }}
                onDateChange={(date) => {setEventDate(date)}}
            />
            <Dialog.Button label="Annuler" onPress={handleCancel}/>
            <Dialog.Button label="Ajouter" onPress={handleAdd}/>
          </Dialog.Container>
        </View>
    );
}

export function EventEditModal(props){
    const [eventName, setEventName] = useState(props.event.name);
    const [eventMessage, setEventMessage] = useState(props.event.message);
    const [eventDate, setEventDate] = useState(props.event.date);

    const handleCancel = () => {
        props.handle_visibility();
    };
    
    const handleModify = () => {
        API.put('/api/events/'+props.event.id, {
            name: eventName,
            message: eventMessage,
            date: eventDate
        })
        .then(() => {
            props.handle_reload();
            props.handle_visibility();
            setEventName("");
            setEventMessage("");
            setEventDate("");
        })
        .catch((err) => {  
            console.log("Event Modify Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    };

    return (
        <View>
          <Dialog.Container visible={props.visible}>
            <Dialog.Title style={{textAlign:'center', marginBottom:5}}>Ajouter un nouvel évenement</Dialog.Title>
            <Dialog.Input onChangeText={setEventName} style={{borderBottomWidth:1, fontSize:19, marginTop:5}} placeholder={"Nom de l'évenement"} value={eventName}></Dialog.Input>
            <TextareaItem
                style={{borderBottomWidth:1, fontSize:19, marginTop:5, marginBottom:30}}
                placeholder="Entrer une description"
                onChange={setEventMessage}
                value={eventMessage}
                autoHeight
            />
            <DatePicker
                style={{width: '100%', display:'flex', marginBottom:10}}
                date={eventDate}
                mode="date"
                format="YYYY-MM-DD"
                minDate="1997-01-01"
                maxDate="2099-12-31"
                useNativeDriver={true}
                customStyles={{
                    dateIcon: {
                        display:'none'
                    }
                }}
                onDateChange={(date) => {setEventDate(date)}}
            />
            <Dialog.Button label="Annuler" onPress={handleCancel}/>
            <Dialog.Button label="Modifier" onPress={handleModify}/>
          </Dialog.Container>
        </View>
    );
}

export function EventView(props){
    const [visible, setVisible] = useState(false)

    const handleDelete = () => {
        API.delete('/api/events/'+props.event.id)
        .then((res) => {
            props.handle_reload();
        })
        .catch((err) => {  
            console.log("Event Delete Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    };

    const handleEditVisibility = () => { setVisible(!visible) }

    return (
        <View style={{width:'100%', flexDirection:'column', marginVertical: 5, paddingVertical:5, paddingHorizontal:5, backgroundColor: 'rgba(255,255,255,0.5)'}}>
            <Text style={{fontSize:18, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1, textAlign:'center'}}>{props.event.name}</Text>
            <Text style={{fontSize:18, color:'#4F4F4F', textAlign:'center'}}>{props.event.date.split("T")[0]}</Text>
            <Text style={{fontSize:18, color:'#4F4F4F', textAlign:'center'}}>{props.event.message}</Text>
            {props.isParent?
            <View style={{flexDirection:'row', paddingHorizontal:5, justifyContent:'center'}}>
                <Button onPress={handleDelete} style={{flex: 0.6, backgroundColor: '#334668', marginVertical: 10, marginHorizontal: 5}}><Text style={{color:'white', fontWeight:'bold'}}>Supprimer</Text></Button>
                <Button onPress={handleEditVisibility} style={{flex: 0.6, backgroundColor: '#334668', marginVertical: 10, marginHorizontal: 5}}><Text style={{color:'white', fontWeight:'bold'}}>Modifier</Text></Button>
            </View>:<></>}
            <EventEditModal handle_visibility={handleEditVisibility} handle_reload={props.handle_reload} visible={visible} event={props.event}/>  
        </View>
    )
}

//Base Class

export default class Event extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            events: [],
            selectedDate: "",
            eventsDates: undefined
        }

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;

        this.loadEvents();
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    handleAddVisibility(){
        if(this._isMounted)
            this.setState({visible: !this.state.visible});
    }

    loadEvents(){
        API.get('/api/events')
        .then((res) => {
            if(this._isMounted){
                this.setState({events: res.data})
            
               var dates = {};

                res.data.forEach(element => {
                    dates[element.date.split("T")[0]] = {marked: true}
                })
                
                this.setState({eventsDates: dates})
            }
        })
        .catch((err) => {  
            console.log("Events Load Error : " + err)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    selectDate(date, event){
        if(this._isMounted)
            this.setState({selectedDate: date})
    }

    render(){
        return (
            <SafeAreaView style={{flex: 1, width:"100%", flexDirection:'column', alignItems:'center'}}>
                {this.state.selectedDate == ""?
                <CalendarList
                    style={{paddingHorizontal:10}}
                    pastScrollRange={50}
                    onDayPress={(day) => this.setState({selectedDate: day.dateString})}
                    futureScrollRange={50}
                    scrollEnabled={true}
                    showScrollIndicator={true}
                    markedDates={this.state.eventsDates}
                />:
                <ScrollView style={{width:'80%'}}>
                    <View style={{flex:1, justifyContent:'center'}}>
                        {this.state.events.filter(event => event.date.includes(this.state.selectedDate)).map((event, index) => {
                            return <EventView key={index} isParent={this.props.member.name == "Parent"} handle_reload={this.loadEvents.bind(this)} event={event} />
                        })}
                        <Button onPress={this.selectDate.bind(this, "")} style={{flex: 0.3, backgroundColor: '#334668', marginVertical: 10, marginHorizontal: 4}}><Text style={{color:'white', fontWeight:'bold'}}>Retour</Text></Button>
                    </View>
                </ScrollView>
                }
                {this.props.member.name == "Parent"?
                <View style={{width:'100%', alignItems:'flex-end', padding:5, position:'absolute', bottom:0}}>
                    <TouchableOpacity onPress={this.handleAddVisibility.bind(this)}>
                        <Image 
                            style={{width:50, height: 50}}
                            resizeMode={'contain'}
                            source={require('../../assets/create_logo.png')}
                        />
                    </TouchableOpacity>
                    <EventAddModal handle_visibility={this.handleAddVisibility.bind(this)} handle_reload={this.loadEvents.bind(this)} visible={this.state.visible}/>
                </View>:<></>}
            </SafeAreaView>
        )
    }
}