import React from 'react';
import API from '../api';

import { View, Text, Alert, Image, ScrollView, TouchableOpacity } from 'react-native';
import { WingBlank, InputItem } from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage'

//Elements

export function MessageCard(props){
    return (
        <>
        {props.message.member_id != props.user_id?
        <View style={{flex: .1, flexDirection:'row', marginHorizontal:10, marginVertical:5}}>
            <View style={{flex:0.1, marginTop:5}}>
                <Text style={{textAlign:'center', fontSize:22, fontWeight:"bold"}}>{props.message.username.slice(0,1)}</Text>
            </View>
            <View style={{flex:0.7, backgroundColor: 'rgba(255,255,255,0.6)', borderRadius:20}}>
                <Text style={{textAlign:'center', fontSize:17, margin:8}}>{props.message.message}</Text>
            </View>
            <WingBlank style={{flex: 0.2}}></WingBlank>
        </View>:
        <View style={{flex: .1, flexDirection:'row', marginHorizontal:10, marginVertical:5}}>
            <WingBlank style={{flex: 0.2}}></WingBlank>
            <View style={{flex:0.7, backgroundColor: 'rgba(255,255,255,0.6)', borderRadius:20}}>
                <Text style={{textAlign:'center', fontSize:17, margin:8}}>{props.message.message}</Text>
            </View>
            <View style={{flex:0.1, marginTop:5}}>
                <Text style={{textAlign:'center', fontSize:22, fontWeight:"bold"}}>{props.message.username.slice(0,1)}</Text>
            </View>
        </View>}
        </>
    )
}

//Base Class

export default class Message extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            message: "",
            messages: []
        }

        this._isMounted = false;
    }

    async componentDidMount(){
        this._isMounted = true;

        try{
            const user = await AsyncStorage.getItem('UserData');
            this.loadMessage()
            if(this._isMounted){
                this.setState({user: JSON.parse(user)});
                this.interval = setInterval(() => this.loadMessage(), 8000);
            }
        } catch(error){
            Alert.alert("Erreur inconnue !")
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
        clearInterval(this.interval);
    }

    async loadMessage(){
        await API.get('/api/messages')
        .then((res) => {
            if(this._isMounted)
                this.setState({messages: res.data})
        })
        .catch((err) => {  
            console.log("Message Load Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    async sendMessage(){
        await API.post('/api/messages', {
            message: this.state.message
        })
        .then((res) => {
            if(this._isMounted){
                this.setState({message: ""})
                this.loadMessage();
            }
        })
        .catch((err) => {
            console.log("Message Post Error : " + err.response.data.message)

            Alert.alert("Erreur inconnue !")
        })
    }

    handleMessageChange(value){
        if(this._isMounted)
            this.setState({message: value});
    }

    render(){
        return (
            <View style={{flex: 0.95, width:'85%', flexDirection:'column', alignItems:'center', backgroundColor: 'rgba(255,255,255,0.5)'}}>
                <ScrollView style={{flex: .9, width:'100%', marginVertical: 10}}>
                    {this.state.messages.map( (message, index) => {
                        return <MessageCard key={index} message={message} user_id={this.state.user.id}/>
                    })}
                </ScrollView >
                <View style={{flex: .1}}>
                    <View style={{flex: 1, width:'100%', flexDirection:'row'}}>
                        <View style={{flex: .8, justifyContent:'center'}}>
                            <InputItem style={{fontSize:15, borderTopWidth:1}} onChange={this.handleMessageChange.bind(this)} value={this.state.message} placeholder="Entrer un message"></InputItem>
                        </View>
                        <View style={{flex: .2, justifyContent:'center', height:'100%'}}>
                            <TouchableOpacity onPress={this.sendMessage.bind(this)} style={{alignItems:'center'}}>
                                <Image 
                                    style={{width:35, height:35}}
                                    resizeMode={'contain'}
                                    source={require('../../assets/send_logo.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}