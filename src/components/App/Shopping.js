import React, { useState } from 'react';
import API from '../api';

import { View, Text, Alert, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { InputItem } from '@ant-design/react-native';
import Dialog from "react-native-dialog";

//Elements

export function ListView(props){
    const [item, setItem] = useState("");

    const sendNewItem = () => {
        if(item != ""){
            const items = (props.list.items != null)?props.list.items:'[]';
            const items_parsed = JSON.parse(items);

            items_parsed.push({"Name":item,"Quantity":1,"Check":false})

            API.put('/api/shopping/'+props.list.id, {
                items: JSON.stringify(items_parsed)
            })
            .then((res) => {
                props.handle_reload();
                setItem("");
            })
            .catch((err) => {
                console.log("Items Post Error : " + err.response.data.message)
    
                switch(err.response.status){
                    case 404 : Alert.alert("Server Error");
                        break;
                    case 400 : Alert.alert("Invalid or Empty Fields")
                        break;
                    default : Alert.alert("Unknown Error")
                        break;
                }
            })
        }
    }

    const changeItem = (id, number, check) => {
        const items = (props.list.items != null)?props.list.items:'[]';
        const items_parsed = JSON.parse(items);

        items_parsed[id].Quantity = items_parsed[id].Quantity + number;
        items_parsed[id].Check = check;

        if(items_parsed[id].Quantity <= 0){
            for( var i = 0; i < items_parsed.length; i++){ 
                if ( i === id) { 
                    items_parsed.splice(i, 1); 
                }
            }
        }

        API.put('/api/shopping/'+props.list.id, {
            items: JSON.stringify(items_parsed)
        })
        .then((res) => {
            props.handle_reload();
        })
        .catch((err) => {
            console.log("Items Post Error : " + err.response.data.message)

            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    }

    const deleteList = () => {
        API.delete('/api/shopping/'+props.list.id)
        .then((res) => {
            props.handle_reload();       
        })
        .catch((err) => {
            console.log("List delete Error : " + err.response.data.message)

            switch(err.response.status){
                case 404 : Alert.alert("Server Error");
                    break;
                case 400 : Alert.alert("Invalid or Empty Fields")
                    break;
                default : Alert.alert("Unknown Error")
                    break;
            }
        })
    }

    return (
        <View style={{flex:1, width:"100%", marginVertical:5, backgroundColor: 'rgba(255,255,255,0.5)'}}>
            <View style={{marginHorizontal:15, marginTop:10, marginBottom:0, flexDirection:'row', justifyContent:'space-between'}}>
                <Text style={{fontWeight:'bold', fontSize:20}}>{props.list.name}</Text>
                {props.isParent?
                <TouchableOpacity onPress={deleteList} style={{alignItems:'center'}}>
                    <Image 
                        style={{width:20, height:20}}
                        resizeMode={'contain'}
                        source={require('../../assets/delete_logo.png')}
                    />
                </TouchableOpacity>:<></>}
            </View>  
            <View style={{flex:1, alignItems:'center', margin:10, backgroundColor:'white', borderRadius:15}}>
                <View style={{margin: 15, width:'90%'}}>
                    {props.list.items != null && JSON.parse(props.list.items).length > 0?<>
                    {JSON.parse(props.list.items).map((item, index) => {
                        return (
                        <View key={index}style={{display:'flex', flexDirection:'row', marginTop:5}}>
                            <View style={{display:'flex', flexDirection:'row', justifyContent:'flex-start', flex:0.8}}>
                                <Text onPress={() => changeItem(index, 0, !item.Check)} style={item.Check?{fontSize:15, marginBottom:2, fontWeight:'bold', textDecorationLine: 'line-through'}:{fontSize:15, marginBottom:2, fontWeight:'bold'}}>{item.Name} x {item.Quantity}</Text>
                            </View>
                            {!item.Check?
                            <View style={{display:'flex', flexDirection:'row', justifyContent:'space-around', flex:0.2}}>
                                <TouchableOpacity onPress={() => changeItem(index, 1, false)} style={{alignItems:'center'}}>
                                <Image 
                                    style={{width:22, height:22}}
                                    resizeMode={'contain'}
                                    source={require('../../assets/add_item.png')}
                                />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => changeItem(index, -1, false)} style={{alignItems:'center'}}>
                                <Image 
                                    style={{width:22, height:22}}
                                    resizeMode={'contain'}
                                    source={require('../../assets/sup_item.png')}
                                />
                                </TouchableOpacity>
                            </View>:<></>}
                        </View>
                        )
                    })}</>:<Text style={{fontSize:15, marginBottom:2, fontWeight:'bold'}}>Aucun Objets</Text>}
                </View>
                <View style={{flex:1, flexDirection:'row', width:'100%'}}>
                    <View style={{flex: .8, justifyContent:'center'}}>
                        <InputItem style={{fontSize:14, borderTopWidth:1}} onChange={setItem} value={item} placeholder="Ajouter un objet"></InputItem>
                    </View>
                    <View style={{flex: .2, justifyContent:'center', height:'100%'}}>
                        <TouchableOpacity onPress={sendNewItem} style={{alignItems:'center'}}>
                            <Image 
                                style={{width:30, height:30}}
                                resizeMode={'contain'}
                                source={require('../../assets/send_logo.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

export function ShoppingAddModal(props){
    const [listname, setListname] = useState("");

    const handleCancel = () => {
        props.handle_visibility();
    };
    
    const handleAdd = () => {
        API.post('/api/shopping', {
            list_name: listname
        })
        .then(() => {
            props.handle_reload();
            setListname("");
        })
        .catch((err) => {  
            console.log("List Add Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })

        props.handle_visibility();
    };

    return (
        <View>
          <Dialog.Container visible={props.visible}>
            <Dialog.Title style={{textAlign:'center', marginBottom:5}}>Ajouter une nouvelle liste</Dialog.Title>
            <Dialog.Input onChangeText={setListname} style={{borderBottomWidth:1, fontSize:19, marginTop:5}} label={"Nom de la liste"} value={listname}></Dialog.Input>
            <Dialog.Button label="Annuler" onPress={handleCancel}/>
            <Dialog.Button label="Ajouter" onPress={handleAdd}/>
          </Dialog.Container>
        </View>
    );
}

//Base Class

export default class Shopping extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            shoppings: []
        }

        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;

        this.loadShopping();
        if(this._isMounted){
            this.interval = setInterval(() => this.loadShopping(), 8000);
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
        clearInterval(this.interval);
    }

    handleAddVisibility(){
        if(this._isMounted)
            this.setState({visible: !this.state.visible});
    }

    loadShopping(){
        console.log("Refreshing shopping !")
        API.get('/api/shopping')
        .then((res) => {
            if(this._isMounted)
                this.setState({shoppings: res.data})
        })
        .catch((err) => {  
            console.log("List Load Error : " + err.response.data.message)
            
            Alert.alert("Erreur lors du chargement de la liste de courses");
        })
    }

    render(){
        return (
            <SafeAreaView style={{flex: 1, width:"100%", flexDirection:'column', alignItems:'center'}}>
                <ScrollView style={{flex: 1, width:"85%"}}>
                    {this.state.shoppings.map((list, index) => {
                        return <ListView key={index} isParent={this.props.member.name=="Parent"} list={list} handle_reload={this.loadShopping.bind(this)}/>
                    })}
                </ScrollView>
                <View style={{width:'100%', alignItems:'flex-end', padding:5, position:'absolute', bottom:0}}>
                    <TouchableOpacity onPress={this.handleAddVisibility.bind(this)}>
                        <Image 
                            style={{width:50, height: 50}}
                            resizeMode={'contain'}
                            source={require('../../assets/create_logo.png')}
                        />
                    </TouchableOpacity>
                    <ShoppingAddModal handle_visibility={this.handleAddVisibility.bind(this)} handle_reload={this.loadShopping.bind(this)} visible={this.state.visible}/>
                </View>
            </SafeAreaView>
        )
    }
}