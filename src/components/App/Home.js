import React, { useState } from 'react';
import API from '../api';
import AsyncStorage from '@react-native-community/async-storage'

import { View, Text, Alert, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';

//Elements

export function ShoppingSetModal(props){
    const [selectedId, setSelectedId] = useState(0);

    const handleCancel = () => {
        props.handle_visibility();
    };
    
    const handleSelect = () => {
       props.handle_selection(selectedId)
       props.handle_visibility();
    };

    return (
        <View>
          <Dialog.Container visible={props.visible}>
            <Dialog.Title style={{textAlign:'center', marginBottom:5}}>Sélectioner une liste à afficher</Dialog.Title>
            <Picker
                selectedValue={selectedId}
                onValueChange={(itemValue, itemIndex) =>
                    setSelectedId(itemValue)
            }>
                {props.lists.map((list, index) => {
                    return <Picker.Item key={index} label={list.name} value={index} />
                })}
            </Picker>
            <Dialog.Button label="Annuler" onPress={handleCancel}/>
            <Dialog.Button label="Ajouter" onPress={handleSelect}/>
          </Dialog.Container>
        </View>
    );
}

export function OneListView(props){
    const changeItem = (id, check) => {
        const items = (props.list.items != null)?props.list.items:'[]';
        const items_parsed = JSON.parse(items);

        items_parsed[id].Check = check;

        API.put('/api/shopping/'+props.list.id, {
            items: JSON.stringify(items_parsed)
        })
        .then((res) => {
            props.handleReload();
        })
        .catch((err) => {
            console.log("Items Post Error : " + err.response.data.message)

            Alert.alert("Erreur inconnue !")
        })
    }

    return (
        <View style={{flex:1, width:"100%", marginVertical:5, backgroundColor: 'rgba(255,255,255,0.5)'}}>
            <View style={{margin:15, marginBottom:0, flexDirection:'row', justifyContent:'space-between'}}>
            <Text style={{fontSize:22, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Courses - <Text style={{fontSize:15}}>{props.list.name}</Text></Text>
                <TouchableOpacity onPress={props.setVisibility} style={{alignItems:'center'}}>
                    <Image 
                        style={{width:20, height:20}}
                        resizeMode={'contain'}
                        source={require('../../assets/contextmenu_logo.png')}
                    />
                </TouchableOpacity>
            </View>  
            <View style={{flex:1, alignItems:'center', margin:15, backgroundColor:'white', borderRadius:15}}>
                <View style={{margin: 15, width:'90%'}}>
                    {props.list.items != null && JSON.parse(props.list.items).length > 0?<>
                    {JSON.parse(props.list.items).map((item, index) => {
                        return (
                        <View key={index}style={{display:'flex', flexDirection:'row'}}>
                            <View style={{display:'flex', flexDirection:'row', justifyContent:'flex-start', flex:0.78}}>
                                <Text onPress={() => changeItem(index, !item.Check)} style={item.Check?{fontSize:18, marginBottom:3, fontWeight:'bold', textDecorationLine: 'line-through'}:{fontSize:18, marginBottom:3, fontWeight:'bold'}}>{item.Name} x {item.Quantity}</Text>
                            </View>
                        </View>
                        )
                    })}</>:<Text style={{fontSize:15, marginBottom:2, fontWeight:'bold'}}>Aucun Objets</Text>}
                </View>
            </View>
        </View>
    )
}

//Base Class

export default class Home extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            shoppings: [],
            selectedList: 0,
            shoppingSet: false
        }

        this._isMounted = false;
    }

    async componentDidMount(){
        this._isMounted = true;

        this.loadShopping();
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    handleSetVisibility(){
        if(this._isMounted)
            this.setState({shoppingSet: !this.state.shoppingSet});
    }

    loadShopping(){
        API.get('/api/shopping')
        .then(async (res) => {
            if(this._isMounted)
                this.setState({shoppings: res.data})

            var id = await AsyncStorage.getItem('ShoppingList');
            
            if(this._isMounted && id && parseInt(id) < this.state.shoppings.length){
                this.setState({selectedList: parseInt(id)});
            }
        })
        .catch((err) => {  
            console.log("List Load Error : " + err.response.data.message)
            
            Alert.alert("Erreur inconnue !")
        })
    }

    async changeSelectedList(value){
        try{
            await AsyncStorage.setItem(
                'ShoppingList',
                value.toString()
            );

            if(this._isMounted)
                this.setState({selectedList: value})
        } catch(error){
            console.log("Storage Error : " + error)
        }
    }

    render(){
        return (
            <SafeAreaView style={{flex: 1, width:"100%", flexDirection:'column', alignItems:'center'}}>
                <ScrollView style={{flex: 1, width:"90%"}}>

                    {this.state.shoppings.length > 0 ?
                        <OneListView list={this.state.shoppings[this.state.selectedList]} setVisibility={this.handleSetVisibility.bind(this)} handleReload={this.loadShopping.bind(this)}/>
                    :<></>}

                </ScrollView>
                <View style={{width:'100%', alignItems:'flex-end', padding:5, position:'absolute', bottom:0}}>
                    <ShoppingSetModal handle_visibility={this.handleSetVisibility.bind(this)} visible={this.state.shoppingSet} lists={this.state.shoppings} handle_selection={this.changeSelectedList.bind(this)}/>            
                </View>
            </SafeAreaView>
        )
    }
}