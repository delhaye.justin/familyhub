import axios from 'axios';

export default axios.create({
    baseURL: 'http://vps-10a8b356.vps.ovh.net:3000',
    withCredentials: true,
    headers: {
        'Content-Type' : 'application/json'
    }
});