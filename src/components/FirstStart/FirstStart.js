import React from 'react';

import Discover from './Discover';
import Easy from './Easy';
import Useful from './Useful'
import { View } from 'react-native';

export default class FirstStart extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            actual_page: 1
        }
    }

    handlePageSwitch(){
        this.setState({actual_page: (this.state.actual_page+1)})
    }

    handleSkip(){
        this.props.skip_handler();
    }

    render(){
        return (
            <View style={{flex: 1}}>
                {this.state.actual_page == 1?<Discover skip_handler={this.handleSkip.bind(this)} continue_handler={this.handlePageSwitch.bind(this)}/>:<></>}
                {this.state.actual_page == 2?<Easy skip_handler={this.handleSkip.bind(this)} continue_handler={this.handlePageSwitch.bind(this)}/>:<></>}
                {this.state.actual_page == 3?<Useful skip_handler={this.handleSkip.bind(this)}/>:<></>}
            </View>
        )
    }
}