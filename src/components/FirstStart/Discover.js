import React from 'react';
import { Button } from '@ant-design/react-native';
import { Text, View } from 'react-native';

export function MainTitle(){
    return (
        <View style={{flex: .6, alignItems:'flex-start', justifyContent:'flex-end'}}>
            <Text style={{fontSize:40, marginLeft:15, color:'#1C356B', fontWeight:'700', letterSpacing:2}}>Simple</Text>
        </View>
    )
}

export function TextDescription(){
    return (
        <View style={{flex: .3}}>
            <Text style={{fontSize:18, marginLeft:15, marginRight:15, color:'#1C356B', fontWeight:'bold', letterSpacing:1}}>L’application Family Hub a été pensée pour que chaque utilisateur puisse l’utiliser de façon simple et efficace !</Text>
        </View>
    )
}

export function Footer(props){
    return (
        <View style={{flex: .1, flexDirection:'row', alignItems:'center'}}>
            <Button onPress={props.skip_handler} style={{flex: 0.25, backgroundColor:'transparent', borderWidth:0}}><Text style={{fontSize:20, marginLeft:10, color:'#1C356B'}}>Passer</Text></Button>
            <View style={{flex: .55}}></View>
            <Button onPress={props.continue_handler} style={{flex: 0.2, backgroundColor:'transparent', borderWidth:0}}><Text style={{fontSize:20, marginLeft:10, fontWeight:'700', color:'#1C356B', letterSpacing:1}}>1/3 ></Text></Button>
        </View>
    )
}

export default function Discover(props){
    return (
        <View style={{backgroundColor:'#F98E90', flex:1}}>
            <MainTitle />
            <TextDescription />
            <Footer skip_handler={props.skip_handler} continue_handler={props.continue_handler}/>
        </View>
    )
}