import React from 'react';

import Create from './Create';
import Join from './Join';
import Retrieve from './Retrieve'
import { View, Text, Image } from 'react-native';
import { Button } from '@ant-design/react-native';

//Elements

export function Menu(props){
    return (
        <View style={{flex: .65, alignItems:'center' }}>
            <View style={{flex: .7, paddingVertical: 50, paddingHorizontal: 20, width: '80%', backgroundColor: 'rgba(255,255,255,0.5)', alignContent:'center', justifyContent:'space-between'}}>
                <Button onPress={props.create_handler} style={{backgroundColor: '#096DD9'}}><Text style={{color:'white', fontSize:18, fontWeight:'bold'}}>Créer une Famille</Text></Button>
                <Button onPress={props.join_handler} style={{backgroundColor: '#096DD9'}}><Text style={{color:'white', fontSize:18, fontWeight:'bold'}}>Rejoindre une Famille</Text></Button>
                <Button onPress={props.retrieve_handler} style={{backgroundColor: '#096DD9'}}><Text style={{color:'white', fontSize:18, fontWeight:'bold'}}>Récupérer une Famille</Text></Button>
            </View>
        </View>
    )
}

export function Logo(){
    return (
        <View style={{flex: .35, justifyContent:'center',alignItems:'center'}}>
            <Image 
                source={require('../../assets/base_logo.png')}
            />
        </View>
    )
}

//Basic Layout

export function Header(){
    return (
        <View style={{flex: .12, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:22, marginTop:20,color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Bienvenue</Text>
        </View>
    )
}

export function Content(props){
    return (
        <View style={{flex: .76, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
            <View style={{flex: 1}}>
                <Logo />
                <Menu create_handler={props.create_handler} join_handler={props.join_handler} retrieve_handler={props.retrieve_handler}/>
            </View>
        </View>
    )
}

export function Footer(){
    return (
        <View style={{flex: .12, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
            
        </View>
    )
}

//Base Class

export default class Welcome extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            actual_page: 0
        }
    }

    handleReturn(){
        this.setState({actual_page: 0});
    }

    handleCreate(){
        this.setState({actual_page: 1});
    }

    handleJoin(){
        this.setState({actual_page: 2});
    }

    handleRetrieve(){
        this.setState({actual_page: 3});
    }

    handleAppLaunch(){
        this.props.app_handler();
    }

    render(){
        return (
            <View style={{flex: 1}}>
                {this.state.actual_page == 0?
                <View style={{flex: 1, backgroundColor:'#A9F2E8'}}>
                    <Header />
                    <Content create_handler={this.handleCreate.bind(this)} join_handler={this.handleJoin.bind(this)} retrieve_handler={this.handleRetrieve.bind(this)}/>
                    <Footer />
                </View>
                :<></>}
                {this.state.actual_page == 1?<Create app_handler={this.handleAppLaunch.bind(this)} return_handler={this.handleReturn.bind(this)}/>:<></>}
                {this.state.actual_page == 2?<Join app_handler={this.handleAppLaunch.bind(this)} return_handler={this.handleReturn.bind(this)}/>:<></>}
                {this.state.actual_page == 3?<Retrieve app_handler={this.handleAppLaunch.bind(this)} return_handler={this.handleReturn.bind(this)}/>:<></>}
            </View>
        )
    }
}