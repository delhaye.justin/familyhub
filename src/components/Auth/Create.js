import React from 'react';
import API from '../api';

import { View, Text, Image, SafeAreaView, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { Button, WingBlank, InputItem } from '@ant-design/react-native';

//Elements

export function FormularTitle(props){
    return (
        <View style={{flex: 0.25, marginTop:'10%', alignContent:'center', justifyContent:'space-between'}}>
            <Logo />
            <Text style={{fontSize:22, paddingTop:15, textAlign:'center', color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Créer une Famille</Text>
            <Text style={{fontSize:13, textAlign:'center', color:'red', fontWeight:'bold', letterSpacing:1}}>{props.error}</Text>
        </View>
    )
}

export function FormularInput(props){
    return (
        <View style={{flex: 0.60, alignContent:'center', paddingBottom: 5, paddingTop:20, justifyContent:'space-between'}}>
            <InputItem style={{backgroundColor:'white', fontSize:15, paddingLeft:15}} onChange={props.name_handler} placeholder="Nom de la Famille"></InputItem>
            <InputItem style={{backgroundColor:'white', fontSize:15, paddingLeft:15}} onChange={props.username_handler} placeholder="Nom Utilisateur"></InputItem>
            <InputItem type="password" style={{backgroundColor:'white', fontSize:15, paddingLeft:15}} onChange={props.password_handler} placeholder="Mot de Passe"></InputItem>
            <InputItem type="password" style={{backgroundColor:'white', fontSize:15, paddingLeft:15}} onChange={props.passwordc_handler} placeholder="Confirmation Mot de Passe"></InputItem>
        </View>
    )
}

export function FormularButton(props){
    return (
        <View style={{flex: 0.15, marginTop:'10%', alignContent:'center', justifyContent:'space-between'}}>
            <Button onPress={props.create_handler} style={{backgroundColor: '#096DD9', borderWidth: 0}}><Text style={{fontSize:22, textAlign:'center', color:'white', fontWeight:'bold', letterSpacing:1}}>Continuer</Text></Button>
        </View>
    )
}

export class Formular extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            error: "",
            name: "",
            username: "",
            password: "",
            password_confirmation: ""
        }
    }

    handleNameUpdate(value){
        this.setState({name: value});
    }

    handleUsernameUpdate(value){
        this.setState({username: value});
    }

    handlePasswordUpdate(value){
        this.setState({password: value});
    }

    handlePasswordConfirmationUpdate(value){
        this.setState({password_confirmation: value});
    }

    checkPasswordStrengh(){
        let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})')

        return strongPassword.test(this.state.password);      
    }

    changeError(value){
        this.setState({error: value});
    }

    handleFamilyCreation(){
        this.changeError("");
        var SharedPreferences = require('react-native-shared-preferences');

        if(this.state.password != "" && this.state.password == this.state.password_confirmation){
            if(this.checkPasswordStrengh()){
                API.post('/api/families', {
                    name: this.state.name,
                    username: this.state.username,
                    password: this.state.password_confirmation
                })
                .then(async (res) => {
                    API.post('/api/sessions', {
                        invite_code: res.data.creator.invite_code,
                        password: this.state.password_confirmation
                    })
                    .then(async (res) => {
                        try{
                            await AsyncStorage.setItem(
                                'UserData',
                                JSON.stringify(res.data)
                            );

                            SharedPreferences.setName("PREFS");
                            SharedPreferences.setItem("UserId",res.data.id);

                            this.props.app_handler();
                        } catch(error){
                            console.log("Storage Error : " + error)
                        }
                    })
                    .catch((err) => {
                        switch(err.response.status){
                            case 404 : this.changeError("Erreur Serveur Essayer plus tard !");
                                break;
                            case 400 : this.changeError("Champs Invalide ou Vide !");
                                break;
                            default : this.changeError("Erreur Inconnue !");                            
                                break;
                        }

                        console.log("Session Error : " + err.response.data.message)
                    })
                })
                .catch((err) => {
                    switch(err.response.status){
                        case 404 : this.changeError("Erreur Serveur Essayer plus tard !");
                            break;
                        case 400 : this.changeError("Champs Invalide ou Vide !");
                            break;
                        default : this.changeError("Erreur Inconnue !");
                            break;
                    }

                    console.log("Family Creation Error : " + err.response.data.message)
                })
            }else{
                this.changeError("Le mot de passe doit faire aux moins 8 caractères et contenir au moins une majuscule et un nombre !");
            }
        }else{
            this.changeError("Mot de Passe Vide ou Différent !");
        }
    }

    render() {
        return (
            <View style={{flex: 1, marginTop: 30, paddingTop: 30, paddingHorizontal: 15, width: '80%', backgroundColor: 'rgba(255,255,255,0.5)', alignContent:'center', justifyContent:'space-between'}}>
                <FormularTitle error={this.state.error}/>
                <FormularInput name_handler={this.handleNameUpdate.bind(this)} username_handler={this.handleUsernameUpdate.bind(this)} password_handler={this.handlePasswordUpdate.bind(this)} passwordc_handler={this.handlePasswordConfirmationUpdate.bind(this)}/>
                <FormularButton create_handler={this.handleFamilyCreation.bind(this)}/>
            </View>
        )
    }
}

export function Logo(){
    return (
        <View style={{flex: .5, justifyContent:'center',alignItems:'center'}}>
            <Image 
                style={{width:60}}
                resizeMode={'contain'}
                source={require('../../assets/create_logo.png')}
            />
        </View>
    )
}

//Basic Layout

export function Header(props){
    return (
        <View style={{flex: .12, flexDirection:'row', justifyContent:'flex-start',alignItems:'center'}}>
            <WingBlank style={{flex: 0.1}}>
                <TouchableOpacity style={{paddingLeft:10}} onPress={props.return_handler}>
                    <Image 
                        style={{width:30, height: 30, alignItems:'center', marginTop:20}}
                        resizeMode={'contain'}
                        source={require('../../assets/back_logo.png')}
                    />
                </TouchableOpacity>
            </WingBlank>
            <Text style={{fontSize:22, textAlign:'center', flex: 0.8, marginTop:20, color:'#4F4F4F', fontWeight:'bold', letterSpacing:1}}>Créer</Text>
            <WingBlank style={{flex: 0.1}}></WingBlank>
        </View>
    )
}

export function Content(props){
    return (
        <View style={{flex: .76, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
            <View style={{flex: 1,  alignItems:'center'}}>
                <Formular app_handler={props.app_handler} />
            </View>
        </View>
    )
}

export function Footer(){
    return (
        <View style={{flex: .12,alignItems:'center'}}>
            <Text style={{fontSize: 11, textAlign:'center', width: '60%', marginTop: 10}}>By creating an account you agree to our Terms of Service and Privacy Policy</Text>
        </View>
    )
}

//Base Class

export default class Create extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            actual_page: 0,
        }
    }

    handleReturn(){
        this.setState({actual_page: 0})
    }

    render(){
        return (
            <SafeAreaView style={{flex: 1, backgroundColor:'#A9F2E8'}}>
                <Header return_handler={this.props.return_handler}/>
                <Content app_handler={this.props.app_handler}/>
                <Footer />
            </SafeAreaView>
        )
    }
}